// Setup the dependencies 
const express = require("express");
const mongoose = require("mongoose");

const taskRoute = require("./routes/taskRoute")

// Server setup
const app = express();
const port = 3001;

//Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Database connection
mongoose.connect("mongodb+srv://laronjhon:admin123@zuitt-bootcamp.tyql6ow.mongodb.net/s36?retryWrites=true&w=majority",
	{	
		useNewUrlParser: true,
		useUnifiedTopology: true
	}	
);

mongoose.connection.once("open", () => console.log(`Now connected to the cloud database`));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Now listening to port ${port}`));